# README #

Another (unofficial) libGDX Scene3D extension.

### Features ###

This extension contains a scene graph for three-dimensional node transformations using the libGDX graphics library (https://libgdx.badlogicgames.com). The following features are currently implemented:

* Local to world transformations of nodes. This means you can make a node a child of another node and it will inherit the parents position/rotation/scale in the world while still applying its own local transformations.
* Local to world axis aligned bounding box calculation. Each node may have its own local bounding box that is also inherited through the node hierarchy.
* Efficient culling: Only nodes without a valid bounding box or with bounds visible to the camera are being rendered
* Node attachments: Each node can have a set of attachments that render models, decals or do some custom stuff with the node (e.g. transformation changes)
* Node event listeners: Nodes can trigger events in certain situations. This aims to be the equivalent to scene2d actor listeners.

### How do I get set up? ###

The sources of this package can be easily copied to your own libGDX project. Have a look at the example code in this repository if you don't know how to use this stuff.
