package de.randomaccess.scene3d;

/**
 * Interface for node attachments.
 */
public interface NodeAttachment {

	/**
	 * Updates the attachment
	 * @param deltaTime 
	 * @param n The node that is triggering the update process
	 * @param isVisible true if this node is currently visible.
	 */
	void update(float deltaTime, Node n, boolean isVisible);

	/**
	 * Called when the attachment is attached to a node.
	 * @param n The node that is triggering the event
	 */
	void attached(Node n);
	
	/**
	 * Called when the attachment is detached from a node.
	 * @param n The node that is triggering the event
	 */
	void detached(Node n);

}
