package de.randomaccess.scene3d;

import com.badlogic.gdx.math.Vector3;

public class SceneQueryResult {

	public Node node;
	public Vector3 intersection;
	
}
