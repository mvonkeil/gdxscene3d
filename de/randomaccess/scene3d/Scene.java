package de.randomaccess.scene3d;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.Queue;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.randomaccess.scene3d.Node.Event;
import de.randomaccess.scene3d.event.TouchEvent;

/**
 * @author void
 *
 *         Scene3D main class. Every scene node is attached to this class. Use
 *         getRoot() to attach nodes to this scene. Call update() to render.
 */
public class Scene implements InputProcessor, GestureListener {

	private Node root;
	ArrayMap<String, Node> nodeNameLookup = new ArrayMap<String, Node>();
	ShapeRenderer boundsRenderer = new ShapeRenderer();
	Queue<Node> boundsToRender = new Queue<Node>();
	boolean inUpdate = false;
	private Viewport viewport;

	/**
	 * Creates a new scene using the given viewport.
	 * 
	 * @param viewport
	 *            a viewport to use for this scene
	 */
	public Scene(Viewport viewport) {
		root = new Node(this);
		this.viewport = viewport;
	}

	/**
	 * @return this scenes viewport
	 */
	public Viewport getViewport() {
		return viewport;
	}

	/**
	 * Sets this scenes viewport.
	 * 
	 * @param viewport
	 *            the new viewport
	 */
	public void setViewport(Viewport viewport) {
		this.viewport = viewport;
	}

	/**
	 * Updates the scene and renders the visible part of the scene graph.
	 * 
	 * @param cam
	 *            the camera to use for frustum checks.
	 */
	public void update(float deltaTime) {
		viewport.apply();
		// viewport.getCamera().update();
		boundsToRender.clear();
		inUpdate = true;
		root.update(deltaTime, viewport.getCamera(), true);
		inUpdate = false;
	}
	
	/**
	 * Renders the debug bounds.
	 */
	public void renderBounds() {
		boundsRenderer.setProjectionMatrix(viewport.getCamera().combined);
		while(boundsToRender.size > 0) {
			Node n = boundsToRender.first();
			n.renderBounds();			
			boundsToRender.removeFirst();
		}
	}

	/**
	 * @return the scene root
	 */
	public Node getRoot() {
		return root;
	}

	public Node findNode(String name) {
		return nodeNameLookup.get(name);
	}
	
	/**
	 * Performs a ray query over the scene.
	 * 
	 * @param ray
	 *            the ray to shoot through the scene (e.g. retrieved by
	 *            camera->getPickRay())
	 * @param result
	 *            an array to store the results in.
	 */
	public void executeRayQuery(Ray ray, Array<SceneQueryResult> result) {
		result.clear();
		executeRayQuery(ray, result, null);
	}

	public static interface SceneQueryFilterCallback {
		public boolean filter(Node n);
	}

	/**
	 * Performs a ray query over the scene.
	 * 
	 * @param ray
	 *            the ray to shoot through the scene (e.g. retrieved by
	 *            camera->getPickRay())
	 * @param result
	 *            an array to store the results in.
	 * @param filter
	 *            A lambda expression or function to use for filtering the
	 *            results.
	 */
	public void executeRayQuery(Ray ray, Array<SceneQueryResult> result, SceneQueryFilterCallback filter) {
		BoundingBox tmpBounds = new BoundingBox();
		Vector3 tmpVec = new Vector3();
		executeRayQuery(ray, root, tmpBounds, tmpVec, result, filter);
	}

	// Query result cache
	private Array<SceneQueryResult> sceneQueryResults = new Array<SceneQueryResult>();

	private void executeRayQuery(Ray ray, Node root, BoundingBox tmpBounds, Vector3 tmpVec,
			Array<SceneQueryResult> result, SceneQueryFilterCallback filter) {
		root.getWorldBounds(tmpBounds, false);

		if (Intersector.intersectRayBounds(ray, tmpBounds, tmpVec)) {
			root.getWorldBounds(tmpBounds, true);
			if (Intersector.intersectRayBounds(ray, tmpBounds, tmpVec)) {
				if (filter == null || filter.filter(root)) {
					SceneQueryResult sqr = new SceneQueryResult();
					sqr.node = root;
					sqr.intersection = tmpVec;
					result.add(sqr);
				}
			}
			for (Node n : root.children) {
				executeRayQuery(ray, n, tmpBounds, tmpVec, result, filter);
			}
		}
	}

	/**
	 * ApplicationListener implementation
	 */

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}
	
	private boolean sendTouchEventToSceneQueryResults(TouchEvent e) {
		for (SceneQueryResult r : sceneQueryResults) {
			e.node = r.node;
			e.hitPoint = r.intersection;
			if (e.node.dispatchEvent(e))
				return true;
		}
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {

		Ray ray = viewport.getPickRay(screenX, screenY);
		executeRayQuery(ray, sceneQueryResults);
		TouchEvent e = new TouchEvent();
		e.down = true;
		e.pointer = pointer;
		e.button = button;
		
		return sendTouchEventToSceneQueryResults(e);
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		Ray ray = viewport.getPickRay(screenX, screenY);
		executeRayQuery(ray, sceneQueryResults);
		TouchEvent e = new TouchEvent();
		e.down = false;
		e.pointer = pointer;
		e.button = button;
		
		return sendTouchEventToSceneQueryResults(e);
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		return touchDown((int) x, (int) y, pointer, button);
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {

		Ray ray = viewport.getPickRay(x, y);
		executeRayQuery(ray, sceneQueryResults);
		TouchEvent e = new TouchEvent();
		e.down = true;
		e.isTap = true;
		e.tapCount = count;
		e.button = button;

		return sendTouchEventToSceneQueryResults(e);
	}

	@Override
	public boolean longPress(float x, float y) {
		Ray ray = viewport.getPickRay(x, y);
		executeRayQuery(ray, sceneQueryResults);
		TouchEvent e = new TouchEvent();
		e.isLongPress = true;

		return sendTouchEventToSceneQueryResults(e);
	}

	// Unsupported gestures
	
	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) { 
		return false;
	}

	@Override
	public void pinchStop() {		
	}

}
