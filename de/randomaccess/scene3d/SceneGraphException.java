package de.randomaccess.scene3d;

public class SceneGraphException extends Exception {

	private static final long serialVersionUID = -8676404050094436264L;

	public SceneGraphException(String message) {
        super(message);
    }
	
}
