package de.randomaccess.scene3d.action;

import com.badlogic.gdx.math.Interpolation;

public class Actions3D {

	public static SequenceAction sequence(Action...actions) {
		return new SequenceAction(actions);
	}
	
	public static SequenceAction loop(int count, Action...actions) {
		return new SequenceAction(actions).loop(count);
	}
	
	public static ParallelAction parallel(Action...actions) {
		return new ParallelAction(actions);
	}
	
	public static DelayAction delay(float delay) {
		return new DelayAction(delay);
	}
	
	public static RemoveAction remove() {
		return new RemoveAction();
	}
	
	public static MoveByAction moveBy(float x, float y, float z, Interpolation interpolation, float duration) {
		return new MoveByAction(x, y, z, interpolation, duration);
	}

	public static MoveToAction moveTo(float x, float y, float z, Interpolation interpolation, float duration) {
		return new MoveToAction(x, y, z, interpolation, duration);
	}

	public static ScaleToAction scaleTo(float x, float y, float z, Interpolation interpolation, float duration) {
		return new ScaleToAction(x, y, z, interpolation, duration);
	}
	
	public static RunnableAction run(Runnable runnable) {
		return new RunnableAction(runnable);
	}
		
	
}
