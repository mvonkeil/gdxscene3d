package de.randomaccess.scene3d.action;

import com.badlogic.gdx.utils.Queue;

import de.randomaccess.scene3d.Node;

public class SequenceAction extends Action {

	private Queue<Action> actions = new Queue<Action>();
	private Queue<Action> nextActions = new Queue<Action>();
	private int loops = 0;
	private int loopCount = 0;

	public SequenceAction(Action... actions) {
		for (Action a : actions)
			this.actions.addLast(a);
	}
	
	public SequenceAction loop(int n) {
		loops = n;
		loopCount = n;
		return this;
	}
	
	@Override
	public void reset() {
		
		for(Action a : actions) {
			a.reset();
		}
		
		while(nextActions.size > 0) {
			actions.addFirst(nextActions.last());
			nextActions.removeLast();
		}
		
		loops = loopCount;
	}

	@Override
	public boolean onUpdate(Node n, boolean isVisible, float deltaTime) {
		if (actions.size == 0)
			return true;
				
		Action a = actions.first();
		if (a.update(n, isVisible, deltaTime)) {
			nextActions.addLast(a);
			a.reset();
			actions.removeFirst();
			if( actions.size == 0 ) {				
				
				if(loops >= 0) {
					if(--loops <= 0)
						return true;
				}	
				
				Queue<Action> tmp = actions;
				actions = nextActions;
				nextActions = tmp;
				nextActions.clear();
								
				return false;
			}
		}
		
		return false;
	}
}
