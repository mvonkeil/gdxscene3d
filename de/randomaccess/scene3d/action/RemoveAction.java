package de.randomaccess.scene3d.action;

import com.badlogic.gdx.Gdx;

import de.randomaccess.scene3d.Node;

public class RemoveAction extends Action {

	@Override
	public boolean onUpdate(Node n, boolean isVisible, float deltaTime) {
		Gdx.graphics.requestRendering();
		n.remove();
		return true;
	}

}
