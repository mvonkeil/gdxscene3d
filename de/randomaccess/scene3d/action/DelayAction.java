package de.randomaccess.scene3d.action;

import de.randomaccess.scene3d.Node;

public class DelayAction extends Action {
	
	private float remaining;
	private float delay;
	
	public DelayAction(float delay) {
		this.remaining = delay;
		this.delay = delay;
	}
	
	@Override
	public boolean onUpdate(Node n, boolean isVisible, float deltaTime) {
		if(this.remaining <= 0)
			return true;
		this.remaining -= deltaTime;
		return false;
	}
	
	@Override
	public void reset() {
		this.remaining = delay;
	}

}
