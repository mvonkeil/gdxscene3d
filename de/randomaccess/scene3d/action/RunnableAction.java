package de.randomaccess.scene3d.action;

import de.randomaccess.scene3d.Node;

public class RunnableAction extends Action {
		
	private Runnable runnable;

	public RunnableAction(Runnable runnable) {
		this.runnable = runnable;
	}
	
	@Override
	public boolean onUpdate(Node n, boolean isVisible, float deltaTime) {
		runnable.run();
		return true;
	}
	


}
