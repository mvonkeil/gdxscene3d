package de.randomaccess.scene3d.action;

import com.badlogic.gdx.math.Interpolation;

import de.randomaccess.scene3d.Node;

public abstract class InterpolatedAction extends Action {
	
	private Interpolation interpolation;
	private float duration;
	private float current = 0;
		
	public InterpolatedAction(Interpolation interpolation, float duration) {
		this.interpolation = interpolation;
		this.duration = duration;
	}
	
	@Override
	public void reset() {
		this.current = 0;
	}
	
	@Override
	public boolean update(Node n, boolean isVisible, float deltaTime) {
		
		current += deltaTime;
		
		if(current >= duration) {
			current = duration;
		}
		if(current < 0) {
			current = 0;
		}			
		
		float d = duration > 0 ? interpolation.apply(current / duration) : 0;
		
		return onUpdate(n, isVisible, d) || current >= duration;
	}

	

}
