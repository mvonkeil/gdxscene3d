package de.randomaccess.scene3d.action;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;

import de.randomaccess.scene3d.Node;

public class MoveToAction extends InterpolatedAction {

	Vector3 target;
	Vector3 offset = null;
	Pool<Vector3> pool;
	
	public MoveToAction(float x, float y, float z, Interpolation interpolation, float duration) {
		super(interpolation, duration);
		this.target = new Vector3(x,y,z);
		pool = Pools.get(Vector3.class);
	}
	
	public MoveToAction(Vector3 delta, Interpolation interpolation, float duration) {
		super(interpolation, duration);
		this.target = new Vector3(delta);
	}

	@Override
	public void reset() {
		this.offset = null;
		super.reset();
	}
	
	@Override
	protected boolean onUpdate(Node n, boolean isVisible, float deltaTime) {
		
		Gdx.graphics.requestRendering();
		
		if(offset == null) {
			offset = n.getLocalPosition(new Vector3());
		}
		
		Vector3 tmp = pool.obtain();
		tmp.set(target).sub(offset).scl(deltaTime).add(offset);
		n.setLocalPosition(tmp);
		pool.free(tmp);
		return false;
	}

}
