package de.randomaccess.scene3d.action;

import com.badlogic.gdx.utils.SnapshotArray;

import de.randomaccess.scene3d.Node;

public class ParallelAction extends Action {

	private SnapshotArray<Action> actions = new SnapshotArray<Action>();

	public ParallelAction(Action... actions) {
		this.actions.addAll(actions);
	}

	@Override
	public boolean onUpdate(Node n, boolean isVisible, float deltaTime) {
		if (actions.size == 0)
			return true;
		
		boolean finished=true;
		
		Object[] ary = actions.begin();
				
		for(int i=0;i<actions.size;++i) {
			Action a = (Action)(ary[i]);
			if(a.update(n, isVisible, deltaTime)) 
				actions.removeValue(a, true);
			else
				finished = false;
		}
		actions.end();
		return finished;
	}
}
