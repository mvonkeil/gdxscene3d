package de.randomaccess.scene3d.action;

import de.randomaccess.scene3d.Node;

public abstract class Action {
	
	public boolean update(Node n, boolean isVisible, float deltaTime) {
		return onUpdate(n, isVisible, deltaTime);
	}
	
	protected abstract boolean onUpdate(Node n, boolean isVisible, float deltaTime);

	public void reset() {		
	}
}
