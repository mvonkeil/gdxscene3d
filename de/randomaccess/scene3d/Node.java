package de.randomaccess.scene3d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.SnapshotArray;

import de.randomaccess.scene3d.event.RemoveEvent;

public class Node {

	// Node graph
	private Scene scene;
	private Node parent;
	SnapshotArray<Node> children = new SnapshotArray<Node>();
	private String name;

	// Local transformation
	private Vector3 localPosition = new Vector3(0, 0, 0);
	private Quaternion localOrientation = new Quaternion().idt();
	private Vector3 localScale = new Vector3(1, 1, 1);
	private BoundingBox localBounds = new BoundingBox();

	// World transformation
	private Vector3 worldPosition = new Vector3(0, 0, 0);
	private Quaternion worldOrientation = new Quaternion().idt();
	private Vector3 worldScale = new Vector3(1, 1, 1);
	private BoundingBox worldBounds = new BoundingBox();
	private Matrix4 worldTransform = new Matrix4();
	private BoundingBox localToWorldBounds = new BoundingBox();

	// invalidation flags
	private boolean invalidatedBounds = true, invalidatedTransform = true;

	// Attachments
	private SnapshotArray<NodeAttachment> attachments = new SnapshotArray<NodeAttachment>();

	// Debugging
	private boolean enableDebugBounds = false;
	private Color debugBoundsColor;

	// Temporarily data structures
	private Quaternion tmpQuat = new Quaternion();
	private Vector3 tmpVec = new Vector3();
	private boolean _wasTransformDirty = true;
	private boolean _wasBoundsDirty = true;

	/**
	 * Creates a new root node. This is called inside the Scene class only.
	 */
	Node(Scene scene) {
		this.scene = scene;
		this.name = "";
		localBounds.inf();
		worldBounds.inf();
		debugBoundsColor = new Color(MathUtils.random(1.0f), MathUtils.random(1.0f), MathUtils.random(1.0f), 1.0f);

	}

	/**
	 * Creates a new node
	 * 
	 * @param parent
	 *            the parent of the new node
	 * @param name
	 *            the name of the new node
	 * @throws SceneGraphException
	 *             if the name is already present in the scene hierarchy.
	 */
	public Node(Node parent, String name) throws SceneGraphException {
		this(parent);
		this.setName(name);
	}

	/**
	 * Creates a new node without an explicit name.
	 * 
	 * @param parent
	 *            the parent of the new node
	 * @throws SceneGraphException 
	 */
	public Node(Node parent) throws SceneGraphException {
		this(parent != null ? parent.scene : null);
		if (parent != null)
			parent.addChild(this);
	}

	/**
	 * @return the scene this node belongs to.
	 */
	public Scene getScene() {
		return scene;
	}

	/**
	 * Removes this node from the scene graph.
	 */
	public void remove() {
		if (parent == null) {
			return;
		}
		
		Event e = new RemoveEvent();
		e.node = this;
		dispatchEvent(e);

		parent.invalidate(true);
		parent.children.removeValue(this, true);
		parent = null;

		if (name != null && scene != null)
			scene.nodeNameLookup.removeKey(name);
	}

	/**
	 * Invalidates the cached world transform and bounds. The invalidation of
	 * transformations will affect any child that is not already flagged as
	 * being invalidated. The invalidation on bounds will affect all parent
	 * nodes.
	 * 
	 * @param boundsOnly
	 *            if set to false only the world transformation will be rebuilt.
	 */
	private void invalidate(boolean boundsOnly) {

		if (!invalidatedBounds) {
			invalidatedBounds = true;
			if (parent != null) {
				parent.invalidate(true);
			}
		}

		if (!boundsOnly && !invalidatedTransform) {
			invalidatedTransform = true;
			for (Node n : children) {
				n.invalidate(false);
			}
		}
	}

	/**
	 * Adds a new child to this node.
	 * 
	 * @param child
	 *            The child node to attach. Any previous parent of this node
	 *            will be detached first.
	 * @throws SceneGraphException 
	 */
	public void addChild(Node child) throws SceneGraphException {

		if (child.parent != null)
			child.remove();

		children.add(child);
		child.parent = this;
		boolean setname = child.scene != scene;
		child.scene = scene;
		if(setname && child.name != null)
			child.setName(child.name);
	}

	/**
	 * Removes all children of this node from the scene graph.
	 */
	public void removeAllChildren() {
		for (Node n : children) {
			n.parent = null;
		}
		children.clear();
		invalidate(true);
	}

	/**
	 * Sets this nodes name.
	 * 
	 * @param name
	 *            The name of the new node.
	 * @throws SceneGraphException
	 *             if the name is already present in the scene hierarchy.
	 */
	public void setName(String name) throws SceneGraphException {

		if (this.scene != null) {
			if (!this.name.isEmpty())
				this.scene.nodeNameLookup.removeKey(this.name);
			if (name != null && !name.isEmpty()) {
				if (this.scene.nodeNameLookup.containsKey(name))
					throw new SceneGraphException(
							"A node with the name '" + name + "' already exists in this scene graph.");
				this.scene.nodeNameLookup.put(name, this);
			}
		}
		this.name = name;
	}

	/**
	 * @return the name of this node.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the local position of this node
	 * 
	 * @param x
	 *            the local x offset
	 * @param y
	 *            the local y offset
	 * @param z
	 *            the local z offset
	 */
	public void setLocalPosition(float x, float y, float z) {
		localPosition.set(x, y, z);
		invalidate(false);
	}

	/**
	 * Sets the local position of this node
	 * 
	 * @param p
	 *            the local offset
	 */
	public void setLocalPosition(Vector3 p) {
		localPosition.set(p);
		invalidate(false);
	}

	/**
	 * @param ret
	 *            an optional Vector3 for storing the result.
	 * @return this nodes local position
	 */
	public Vector3 getLocalPosition(Vector3 ret) {
		if (ret == null)
			ret = new Vector3();
		ret.set(localPosition);
		return ret;
	}

	/**
	 * @return this nodes local position x component.
	 */
	public float getLocalPositionX() {
		return localPosition.x;
	}

	/**
	 * @return this nodes local position y component.
	 */
	public float getLocalPositionY() {
		return localPosition.y;
	}

	/**
	 * @return this nodes local position z component.
	 */
	public float getLocalPositionZ() {
		return localPosition.z;
	}

	/**
	 * @param ret
	 *            an optional Vector3 for storing the result.
	 * @return this nodes world position
	 */
	public Vector3 getWorldPosition(Vector3 ret) {
		if (invalidatedTransform)
			updateTransform();

		if (ret == null)
			ret = new Vector3();
		ret.set(worldPosition);
		return ret;
	}

	/**
	 * @return this nodes world position x component.
	 */
	public float getWorldPositionX() {
		if (invalidatedTransform)
			updateTransform();
		return worldPosition.x;
	}

	/**
	 * @return this nodes world position y component.
	 */
	public float getWorldPositionY() {
		if (invalidatedTransform)
			updateTransform();
		return worldPosition.y;
	}

	/**
	 * @return this nodes world position z component.
	 */
	public float getWorldPositionZ() {
		if (invalidatedTransform)
			updateTransform();
		return worldPosition.z;
	}

	/**
	 * Sets the local orientation of this node.
	 * 
	 * @param o
	 *            the local orientation
	 */
	public void setLocalOrientation(Quaternion o) {
		localOrientation.set(o);
		invalidate(false);
	}

	/**
	 * Sets the local orientation of this node from euler angles
	 * 
	 * @param yaw
	 *            yaw component of rotation in degrees
	 * @param pitch
	 *            pitch component of rotation in degrees
	 * @param roll
	 *            roll component of rotation in degrees
	 */
	public void setLocalEulerAngles(float yaw, float pitch, float roll) {
		localOrientation.setEulerAngles(yaw, pitch, roll);
		invalidate(false);
	}

	/**
	 * @param ret
	 *            an optional Quaternion for storing the result.
	 * @return this nodes local orientation
	 */
	public Quaternion getLocalOrientation(Quaternion ret) {
		if (ret == null)
			ret = new Quaternion();
		ret.set(localOrientation);
		return ret;
	}

	/**
	 * @param ret
	 *            an optional Quaternion for storing the result.
	 * @return this nodes world orientation
	 */
	public Quaternion getWorldOrientation(Quaternion ret) {
		if (invalidatedTransform)
			updateTransform();

		if (ret == null)
			ret = new Quaternion();
		ret.set(worldOrientation);
		return ret;
	}

	/**
	 * Sets this nodes local bounds
	 * 
	 * @param bounds
	 *            the local bounds
	 */
	public void setLocalBounds(BoundingBox bounds) {
		this.localBounds.set(bounds);
		invalidate(true);
	}

	/**
	 * Sets this nodes local bounds
	 * 
	 * @param min
	 *            local bounds minimum
	 * @param max
	 *            local bounds maximum
	 */
	public void setLocalBounds(Vector3 min, Vector3 max) {
		this.localBounds.set(min, max);
		invalidate(true);
	}

	/**
	 * Sets this nodes local bounds
	 * 
	 * @param minx
	 *            the bounding boxes minimum x component
	 * @param miny
	 *            the bounding boxes minimum y component
	 * @param minz
	 *            the bounding boxes minimum z component
	 * @param maxx
	 *            the bounding boxes maximum x component
	 * @param maxy
	 *            the bounding boxes maximum y component
	 * @param maxz
	 *            the bounding boxes maximum z component
	 */
	public void setLocalBounds(float minx, float miny, float minz, float maxx, float maxy, float maxz) {
		this.localBounds.min.set(minx, miny, minz);
		this.localBounds.max.set(maxx, maxy, maxz);
		invalidate(true);
	}

	/**
	 * @param ret
	 *            an optional BoundingBox for storing the result.
	 * @return this nodes local bounds
	 */
	public BoundingBox getLocalBounds(BoundingBox ret) {
		if (ret == null)
			ret = new BoundingBox();
		ret.set(localBounds);
		return ret;
	}

	/**
	 * @param ret
	 *            an optional Vector3 for storing the result.
	 * @return this nodes local bounds minimum
	 */
	public Vector3 getLocalBoundsMin(Vector3 ret) {
		if (ret == null)
			ret = new Vector3();
		ret.set(localBounds.min);
		return ret;
	}

	/**
	 * @param ret
	 *            an optional Vector3 for storing the result.
	 * @return this nodes local bounds maximum
	 */
	public Vector3 getLocalBoundsMax(Vector3 ret) {
		if (ret == null)
			ret = new Vector3();
		ret.set(localBounds.max);
		return ret;
	}

	public float getLocalBoundsWidth() {
		return localBounds.getWidth();
	}

	public float getLocalBoundsHeight() {
		return localBounds.getHeight();
	}

	public float getLocalBoundsDepth() {
		return localBounds.getDepth();
	}

	/**
	 * @param ret
	 *            an optional BoundingBox for storing the result.
	 * @param excludeChildren
	 *            if true only this nodes transformed local bounds will be
	 *            returned
	 * @return this nodes world bounds
	 */
	public BoundingBox getWorldBounds(BoundingBox ret, boolean excludeChildren) {
		if (invalidatedBounds)
			updateBounds();

		if (ret == null)
			ret = new BoundingBox();
		ret.set(excludeChildren ? localToWorldBounds : worldBounds);
		return ret;
	}

	public BoundingBox getWorldBounds(BoundingBox ret) {
		return getWorldBounds(ret, false);
	}

	/**
	 * @param ret
	 *            an optional Vector3 for storing the result.
	 * @param excludeChildren
	 *            if true only this nodes transformed local bounds will be
	 *            returned
	 * @return this nodes world bounds minimum
	 */
	public Vector3 getWorldBoundsMin(Vector3 ret, boolean excludeChildren) {
		if (ret == null)
			ret = new Vector3();
		ret.set(excludeChildren ? localToWorldBounds.min : worldBounds.min);
		return ret;
	}

	public Vector3 getWorldBoundsMin(Vector3 ret) {
		return getWorldBoundsMin(ret, false);
	}

	/**
	 * @param ret
	 *            an optional Vector3 for storing the result.
	 * @param excludeChildren
	 *            if true only this nodes transformed local bounds will be
	 *            returned
	 * @return this nodes world bounds minimum
	 */
	public Vector3 getWorldBoundsMax(Vector3 ret, boolean excludeChildren) {
		if (ret == null)
			ret = new Vector3();
		ret.set(excludeChildren ? localToWorldBounds.max : worldBounds.max);
		return ret;
	}

	public Vector3 getWorldBoundsMax(Vector3 ret) {
		return getWorldBoundsMax(ret, false);
	}

	public float getWorldBoundsWidth() {
		return worldBounds.getWidth();
	}

	public float getWorldBoundsHeight() {
		return worldBounds.getHeight();
	}

	public float getWorldBoundsDepth() {
		return worldBounds.getDepth();
	}

	/**
	 * Sets the local scale of this node
	 * 
	 * @param x
	 *            the local x scale
	 * @param y
	 *            the local y scale
	 * @param z
	 *            the local z scale
	 */
	public void setLocalScale(float x, float y, float z) {
		localScale.set(x, y, z);
		invalidate(false);
	}

	/**
	 * Sets this nodes local scale
	 * 
	 * @param s
	 *            the local scale
	 */
	public void setLocalScale(Vector3 s) {
		localScale.set(s);
		invalidate(false);
	}

	/**
	 * @param ret
	 *            an optional Vector3 for storing the result.
	 * @return this nodes local scale.
	 */
	public Vector3 getLocalScale(Vector3 ret) {
		if (ret == null)
			ret = new Vector3();
		ret.set(localScale);
		return ret;
	}

	/**
	 * @return this nodes local scale x component
	 */
	public float getLocalScaleX() {
		return localScale.x;
	}

	/**
	 * @return this nodes local scale y component
	 */
	public float getLocalScaleY() {
		return localScale.y;
	}

	/**
	 * @return this nodes local scale z component
	 */
	public float getLocalScaleZ() {
		return localScale.z;
	}

	/**
	 * @param ret
	 *            an optional Vector3 for storing the result.
	 * @return this nodes world scale
	 */
	public Vector3 getWorldScale(Vector3 ret) {
		if (invalidatedTransform)
			updateTransform();

		if (ret == null)
			ret = new Vector3();
		ret.set(worldScale);
		return ret;
	}

	/**
	 * @return this nodes world scale x component
	 */
	public float getWorldScaleX() {
		return worldScale.x;
	}

	/**
	 * @return this nodes world scale y component
	 */
	public float getWorldScaleY() {
		return worldScale.y;
	}

	/**
	 * @return this nodes world scale z component
	 */
	public float getWorldScaleZ() {
		return worldScale.z;
	}

	/**
	 * Rotates this node around the euler yaw axis
	 * 
	 * @param angle
	 *            the rotation in degrees.
	 */
	public void yaw(float angle) {
		tmpQuat.idt();
		tmpQuat.setEulerAngles(angle, 0, 0);
		localOrientation.mul(tmpQuat);
		invalidate(false);
	}

	/**
	 * Rotates this node around the euler pitch axis
	 * 
	 * @param angle
	 *            the rotation in degrees.
	 */
	public void pitch(float angle) {
		tmpQuat.idt();
		tmpQuat.setEulerAngles(0, angle, 0);
		localOrientation.mul(tmpQuat);
		invalidate(false);
	}

	/**
	 * Rotates this node around the euler roll axis
	 * 
	 * @param angle
	 *            the rotation in degrees.
	 */
	public void roll(float angle) {
		tmpQuat.idt();
		tmpQuat.setEulerAngles(0, 0, angle);
		localOrientation.mul(tmpQuat);
		invalidate(false);
	}

	public boolean isTransformDirty() {
		return _wasTransformDirty;
	}

	public boolean isBoundingBoxDirty() {
		return _wasBoundsDirty;
	}
	
	
	public boolean isDirty() {
		return _wasBoundsDirty || _wasTransformDirty;
	}

	/**
	 * Recalculates the cached world transformations
	 */
	private void updateTransform() {
		if (!invalidatedTransform)
			return;
		
		invalidatedTransform = false;

		worldPosition.set(localPosition);
		worldOrientation.set(localOrientation);
		worldScale.set(localScale);

		if (parent != null) {
			worldPosition.add(parent.worldPosition);
			worldOrientation.mul(parent.worldOrientation);
			worldScale.scl(parent.worldScale);
		}

		worldTransform.set(worldPosition, worldOrientation, worldScale);

		invalidatedBounds = true;

		for (Node n : children)
			n.updateTransform();
	}

	/**
	 * Recalculates the world aligned bounding box
	 */
	private void updateBounds() {
		if (!invalidatedBounds)
			return;
		invalidatedBounds = false;

		if (!localBounds.isValid())
			localBounds.clr();

		worldBounds.set(localBounds);
		worldBounds.mul(worldTransform);
		
		for (Node n : children) {
			n.updateBounds();
			worldBounds.ext(n.worldBounds);
		}
		
		localToWorldBounds.set(worldBounds);
	}

	/**
	 * Updates the node
	 * @param deltaTime 
	 * 
	 * @param cam
	 *            the camera to apply to this node
	 * @param isVisible
	 *            true if the parents bounds were in the camera frustum or
	 *            invalid.
	 */
	protected void update(float deltaTime, Camera cam, boolean isVisible) {
		
		// Cache these variables for eventual later access by custom attachments.
		_wasTransformDirty = invalidatedTransform;
		_wasBoundsDirty = invalidatedBounds;
		
		updateTransform();
		updateBounds();
		
		if(isDirty()) {
			// Non-continuous rendering: If the node was updated, request rendering
			Gdx.graphics.requestRendering();
		}
				
		if (isVisible && worldBounds.isValid() && worldBounds.getWidth() > 0 && worldBounds.getHeight() > 0 && worldBounds.getDepth() > 0) {
			isVisible = cam.frustum.boundsInFrustum(worldBounds);
		}

		for (NodeAttachment a : attachments) {
			a.update(deltaTime, this, isVisible);
		}
		
		for (Node n : children) {
			n.update(deltaTime, cam, isVisible);
		}
		
		if (isVisible && enableDebugBounds && scene != null) {
			scene.boundsToRender.addLast(this);
		}
	}

	void renderBounds() {
		scene.boundsRenderer.begin(ShapeType.Line);
		scene.boundsRenderer.setColor(debugBoundsColor);
		localToWorldBounds.getCorner001(tmpVec);
		scene.boundsRenderer.box(tmpVec.x, tmpVec.y, tmpVec.z, localToWorldBounds.getWidth(),
				localToWorldBounds.getHeight(), localToWorldBounds.getDepth());
		scene.boundsRenderer.end();
	}

	/**
	 * Attaches a new component to this node.
	 * 
	 * @param attachment
	 *            The new attachment
	 * @throws SceneGraphException
	 *             if the attachment is already attached to this node.
	 */
	public <T extends NodeAttachment> T attach(T attachment) throws SceneGraphException {
		if (attachments.contains(attachment, true)) {
			throw new SceneGraphException("Already attached to this node!");
		}

		attachment.attached(this);
		attachments.add(attachment);
		return attachment;
		
	}

	/**
	 * Detaches a component from this node
	 * 
	 * @param attachment
	 *            the attachment to remove.
	 * @throws SceneGraphException
	 *             if the attachment is not attached to this node.
	 */
	public void detach(NodeAttachment attachment) throws SceneGraphException {
		if (!attachments.contains(attachment, true)) {
			throw new SceneGraphException("Not attached to this node!");
		}

		attachment.detached(this);
		attachments.removeValue(attachment, true);
	}

	/**
	 * Detaches all attachments of a given type
	 * 
	 * @param attachment
	 *            a Class type to specify what attachments to remove.
	 */
	public void detachAll(Class<?> attachment) {
		
		Object[] ary = this.attachments.begin();
		
		for (int i=0;i<this.attachments.size;++i) {
			NodeAttachment a = (NodeAttachment)(ary[i]);
			if (attachment.isAssignableFrom(a.getClass())) {
				this.attachments.removeValue(a, true);
			}
		}
		this.attachments.end();
	}
	
	@SuppressWarnings("unchecked")
	public <T extends NodeAttachment> T getAttachment(Class<T> cl) {
		Object[] ary = attachments.begin();
		for (Object o : ary) {
			NodeAttachment a = (NodeAttachment)o;
			if (cl.isAssignableFrom(a.getClass())) {
				attachments.end();
				return (T)a;
			}
		}
		attachments.end();
		return null;
	}

	/**
	 * Detaches all attachments
	 */
	public void detachAll() {
		attachments.clear();
	}

	/**
	 * Enables or disables the rendering of the nodes world boundaries.
	 * 
	 * @param enabled
	 *            true if the world boundaries should be rendered
	 * @param cascade
	 *            true to propagate this setting to all children.
	 */
	public void showDebugBounds(boolean enabled, boolean cascade) {
		enableDebugBounds = enabled;
		if (cascade) {
			for (Node n : children) {
				n.showDebugBounds(enabled, cascade);
			}
		}
	}

	/**
	 * @return this nodes parent.
	 */
	public Node getParent() {
		return parent;
	}

	/**
	 * @return this nodes number of children
	 */
	public int getNumChildren() {
		return children.size;
	}

	/**
	 * @param index
	 *            Get index of the child
	 * @return the given child of this node
	 */
	public Node getChild(int index) {
		return children.get(index);
	}

	/**
	 * Event class
	 */
	public static class Event {
		public Node node;
		private boolean propagate;

		public void stopPropagation() {
			this.propagate = false;
		}

	}

	/**
	 * Event listener interface for node events.
	 */
	public static interface EventListener {
		/**
		 * Handles an event.
		 * 
		 * @return true if the event shouldn't be propagated any further (has
		 *         been handled by this listener), otherwise false.
		 */
		public boolean onEvent(Event e);
	}

	// Listeners
	private Array<EventListener> listeners = new Array<EventListener>();

	/**
	 * Adds a new event listener to this node.
	 * 
	 * @param l
	 *            the new listener
	 */
	public void addEventListener(EventListener l) {
		listeners.add(l);
	}

	/**
	 * Removes an event listener from this node.
	 * 
	 * @param l
	 *            the event listener to remove
	 */
	public void removeEventListener(EventListener l) {
		listeners.removeValue(l, true);
	}

	/**
	 * Removes all event listeners from this node.
	 */
	public void removeAllEventListeners() {
		listeners.clear();
	}

	/**
	 * Dispatches an event. Called by scene manager.
	 * 
	 * @param e
	 *            the event
	 * @return true if the event shouldn't be propagated any further, otherwise
	 *         false.
	 */
	boolean dispatchEvent(Event e) {
		for (EventListener l : listeners) {
			if (l.onEvent(e)) {
				return true;
			}
		}
		return false;
	}
}
