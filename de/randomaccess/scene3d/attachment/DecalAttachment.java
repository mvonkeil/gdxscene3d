package de.randomaccess.scene3d.attachment;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g3d.decals.Decal;
import com.badlogic.gdx.graphics.g3d.decals.DecalBatch;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.utils.Align;

import de.randomaccess.scene3d.Node;
import de.randomaccess.scene3d.NodeAttachment;

/*
 * A node attachment for rendering decals.
 */
public class DecalAttachment implements NodeAttachment {

	public final float BoundingBoxDepth = 0.1f;
	private Decal decal;
	private Vector3 tmpVec = new Vector3();
	private Quaternion tmpQuat = new Quaternion();
	private DecalBatch batch;
	private boolean extendLocalBounds = false;
	private int align;
	private int lastAnimatedFrameIndex = -1;

	/**
	 * Constructor
	 * 
	 * @param decal The decal to attach
	 * @param batch the decal batch to use for rendering.
	 */	
	public DecalAttachment(Decal decal, DecalBatch batch, int align) {
		this.decal = decal;
		this.batch = batch;
		this.align = align;
	}
	
	public DecalAttachment(Decal decal, DecalBatch batch) {
		this(decal, batch, Align.center);
	}

	public Decal getDecal() {
		return decal;
	}

	@Override
	public void update(float deltaTime, Node n, boolean isVisible) {
		
		if (!isVisible) // Only render if this node is visible to the camera
			return;
	
		float halfWidth = decal.getWidth() * 0.5f;
		float halfHeight = decal.getHeight() * 0.5f;
		
		
		float ox = 0, oy = 0;
		
		if((align | Align.bottom) == Align.bottom) {
			oy += halfHeight;
		}
		else if((align | Align.top) == Align.top) {
			oy -= halfHeight;
		}
		if((align | Align.left) == Align.left) {
			ox += halfWidth;
		}
		else if((align | Align.right) == Align.right) {
			ox -= halfWidth;
		}
				
		// Update the decal
		tmpQuat = n.getWorldOrientation(tmpQuat);
		decal.setRotation(tmpQuat);
		decal.setPosition(n.getWorldPosition(tmpVec).add(ox,oy,0));
		
		n.getWorldScale(tmpVec);
		decal.setScale(tmpVec.x, tmpVec.y);

		if (extendLocalBounds) {
			tmpVec.set(halfWidth, halfHeight, BoundingBoxDepth * 0.5f);
			tmpVec.x += n.getLocalPositionX();
			tmpVec.y += n.getLocalPositionY();
			tmpVec.z += n.getLocalPositionZ();
			BoundingBox bounds = new BoundingBox();
			n.getLocalBounds(bounds);
			bounds.ext(tmpVec);
			tmpVec.set(-halfWidth, -halfHeight, BoundingBoxDepth * 0.5f);
			tmpVec.x += n.getLocalPositionX();
			tmpVec.y += n.getLocalPositionY();
			tmpVec.z += n.getLocalPositionZ();
			bounds.ext(tmpVec);
			n.setLocalBounds(bounds);
		}

		if (this.batch != null) {
			batch.add(decal);
		}
	}
	
	/**
	 * Enables or disables the automatic bounding box extension.
	 * @param enabled if set to true the local bounds will be extended by the decal dimensions.
	 */
	public void extendLocalBounds(boolean enabled) {

		this.extendLocalBounds = enabled;
	}

	@Override
	public void attached(Node n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void detached(Node n) {
		// TODO Auto-generated method stub
		
	}

}
