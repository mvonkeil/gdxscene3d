package de.randomaccess.scene3d.attachment;

import com.badlogic.gdx.Gdx;

import de.randomaccess.scene3d.Node;
import de.randomaccess.scene3d.NodeAttachment;
import de.randomaccess.scene3d.action.Action;

public class ActionAttachment implements NodeAttachment {

	@Override
	public void update(float deltaTime, Node n, boolean isVisible) {
		if(root!= null) {
			if(root.update(n, isVisible, deltaTime)) {
				root = null;
			}
		}
	}

	@Override
	public void attached(Node n) {
		n.detachAll(ActionAttachment.class);
	}

	@Override
	public void detached(Node n) {

	}
	
	private Action root = null;

	public <T extends Action> T play(T a) {
		root = a;
		return a;
	}
	
}
