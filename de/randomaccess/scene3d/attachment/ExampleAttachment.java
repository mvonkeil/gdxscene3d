package de.randomaccess.scene3d.attachment;

import de.randomaccess.scene3d.Node;
import de.randomaccess.scene3d.NodeAttachment;

public class ExampleAttachment implements NodeAttachment {

	@Override
	public void update(float deltaTime, Node n, boolean isVisible) {
		n.yaw(3f * deltaTime);
		n.pitch(4f * deltaTime);
		
	}

	@Override
	public void attached(Node n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void detached(Node n) {
		// TODO Auto-generated method stub
		
	}

	

}
