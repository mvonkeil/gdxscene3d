package de.randomaccess.scene3d.attachment;

import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;

import de.randomaccess.scene3d.Node;
import de.randomaccess.scene3d.NodeAttachment;

/*
 * A node attachment for rendering model instances.
 */
public class ModelAttachment implements NodeAttachment {

	private ModelInstance modelInstance;
	private ModelBatch batch;
	private Vector3 tmpVec = new Vector3();
	private Quaternion tmpQuat = new Quaternion();
	private BoundingBox bounds = new BoundingBox();
	private BoundingBox tmpBounds = new BoundingBox();
	private boolean extendLocalBounds;

	/**
	 * Constructor
	 * @param mi the model to render
	 * @param batch the ModelBatch to use for this model instance.
	 */
	public ModelAttachment(ModelInstance mi, ModelBatch batch) {
		this.modelInstance = mi;
		this.batch = batch;
	}
	
	/**
	 * @return the model instance of this attachment
	 */
	public ModelInstance getModelInstance() {
		return modelInstance;
	}
	
	@Override
	public void update(float deltaTime, Node n, boolean isVisible) {
		
		if(!isVisible) // Only render if this node is visible to the camera
			return;
			
		// Update the model instance transformation matrix
		modelInstance.transform.idt();
		bounds = modelInstance.calculateBoundingBox(bounds);
		
		modelInstance.transform.rotate(n.getWorldOrientation(tmpQuat));
		modelInstance.transform.translate(n.getWorldPosition(tmpVec));
		modelInstance.transform.scl(n.getWorldScale(tmpVec));
		
		if(extendLocalBounds) {
			tmpBounds = n.getLocalBounds(tmpBounds);
			
			tmpVec.set(bounds.min);
			tmpVec.x += n.getLocalPositionX();
			tmpVec.y += n.getLocalPositionY();
			tmpVec.z += n.getLocalPositionZ();
			
			tmpBounds.ext(tmpVec);
			
			tmpVec.set(bounds.max);
			tmpVec.x += n.getLocalPositionX();
			tmpVec.y += n.getLocalPositionY();
			tmpVec.z += n.getLocalPositionZ();
			
			tmpBounds.ext(tmpVec);
			
			n.setLocalBounds(tmpBounds);
			
		}
				
		if(this.batch != null) {
			batch.render(modelInstance);
		}
	}
	
	/**
	 * Enables or disables the automatic bounding box extension.
	 * @param enabled if set to true the local bounds will be extended by the model dimensions.
	 */
	public void extendLocalBounds(boolean enabled) {

		this.extendLocalBounds = enabled;
	}

	@Override
	public void attached(Node n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void detached(Node n) {
		// TODO Auto-generated method stub
		
	}
	

}
