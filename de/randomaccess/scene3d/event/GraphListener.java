package de.randomaccess.scene3d.event;

import de.randomaccess.scene3d.Node.Event;
import de.randomaccess.scene3d.Node.EventListener;

public class GraphListener implements EventListener {

	@Override
	public boolean onEvent(Event event) {
		if(event instanceof RemoveEvent) {
			RemoveEvent e = (RemoveEvent)event;
			onRemove(e);
		}
		return false;
	}
	
	public boolean onRemove(RemoveEvent e) {
		return false;
	}
	
}
