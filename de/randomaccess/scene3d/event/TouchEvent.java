package de.randomaccess.scene3d.event;

import com.badlogic.gdx.math.Vector3;

import de.randomaccess.scene3d.Node.Event;

public class TouchEvent extends Event {
	public boolean down;
	public boolean isTap;
	public boolean isLongPress;
	public int tapCount;
	public Vector3 hitPoint;
	public int button;
	public int pointer;
}
