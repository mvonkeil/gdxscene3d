package de.randomaccess.scene3d.event;

import de.randomaccess.scene3d.Node.Event;
import de.randomaccess.scene3d.Node.EventListener;

public class TouchListener implements EventListener {

	@Override
	public boolean onEvent(Event event) {
		if(event instanceof TouchEvent) {
			TouchEvent e = (TouchEvent)event;
			if(e.isTap)
				return onTap(e);
			else if(e.isLongPress)
				return onLongPress(e);
			else if(e.down)
				return onTouchDown(e);
			else
				return onTouchUp(e);
		}
		return false;
	}
	
	public boolean onTouchDown(TouchEvent e) {
		return false;
	}
	
	public boolean onTouchUp(TouchEvent e) {
		return false;
	}
	
	public boolean onTap(TouchEvent e) {
		return false;
	}
	
	public boolean onLongPress(TouchEvent e) {
		return false;
	}

}
