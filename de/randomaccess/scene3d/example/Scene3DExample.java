package de.randomaccess.scene3d.example;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.decals.CameraGroupStrategy;
import com.badlogic.gdx.graphics.g3d.decals.Decal;
import com.badlogic.gdx.graphics.g3d.decals.DecalBatch;
import com.badlogic.gdx.graphics.g3d.decals.DecalMaterial;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import de.randomaccess.scene3d.Node;
import de.randomaccess.scene3d.Scene;
import de.randomaccess.scene3d.Scene.SceneQueryFilterCallback;
import de.randomaccess.scene3d.SceneGraphException;
import de.randomaccess.scene3d.SceneQueryResult;
import de.randomaccess.scene3d.attachment.DecalAttachment;
import de.randomaccess.scene3d.attachment.ExampleAttachment;

public class Scene3DExample extends ApplicationAdapter implements InputProcessor {

	private PerspectiveCamera cam;

	/**
	 * This is our main scene. It will be initialized and populated in create().
 	 */
	Scene scene;

	@Override
	public void create() {

		/**
		 * Create a camera, load the default badlogic logo image and create decal batch. Nothing too fancy.
         */ 
		cam = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		cam.position.set(0, 6, 5);
		cam.lookAt(0, 0, 0);
		cam.near = 1f;
		cam.far = 300f;
		cam.update();

		decalBatch = new DecalBatch(new CameraGroupStrategy(cam));

		TextureRegion img = new TextureRegion(new Texture("badlogic.jpg"));
		
		// Create our scene and make 5 child nodes.
		scene = new Scene(new ScreenViewport(cam));
		Node last = scene.getRoot();

		for (int i = 0; i < 5; ++i) {

			try {
				Decal decal = Decal.newDecal(1, 1, img); // Make a new decal for this node
				Node n = new Node(last);// Add the new node as child of the last inserted node (or the scene root if this is the first one).
				DecalAttachment da = n.attach(new DecalAttachment(decal, decalBatch)); // A decal attachment is used to render decals transformed in the scene graph.
				da.extendLocalBounds(true); // Let the decal attachment calculate the bounding box of this node for us.
				n.attach(new ExampleAttachment()); // The example attachment will spin the nodes around to demonstrate inherited transformations.
				n.setLocalPosition(0,0.8f,0); // Move our local position by 0.8 units on the y axis
				n.setLocalScale(0.75f,0.75f,0.75f); // This will result in a pyramid like structure
				
				last = n; // Rememver the last node we inserted into the scene.
			} catch (SceneGraphException e) {
				e.printStackTrace();
			}

		}
		
		scene.getRoot().showDebugBounds(true, true); // Make all nodes render their axis aligned bounding boxes.
		
		Gdx.input.setInputProcessor(this);
	}
	
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		scene.getViewport().update(width, height);
	}

	private DecalBatch decalBatch;

	float i = 0;

	@Override
	public void render() {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		Gdx.gl.glEnable(GL20.GL_DEPTH_TEST);

		// Move the camera around a little.
		cam.position.x = (float) (Math.sin(i * 0.5f) * 1);

		// Now update the scene
		scene.update(Gdx.graphics.getDeltaTime());
		i += Gdx.graphics.getDeltaTime();

		// We are rendering decals, so the decal batch has to be flushed.
		decalBatch.flush();
		
		// Render scene bounds
		scene.renderBounds();
	}

	@Override
	public void dispose() {
		decalBatch.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {

		// This demonstrates how to use scene ray queries.
		
		Ray ray	= cam.getPickRay(screenX, screenY);
		Array<SceneQueryResult> result = new Array<SceneQueryResult>();

		// Get all selected nodes that have no children (on other words: always select just the leaf nodes of the scene graph)
		
		// Java 8 lambda syntax. Much cleaner, but not supported by android yet.
		// scene.executeRayQuery(ray, result, x -> x.getNumChildren() == 0); 
		
		// Regular Java 6/7 anonymous class syntax.
		scene.executeRayQuery(ray,  result, new SceneQueryFilterCallback() {
			@Override
			public boolean filter(Node n) {
				return n.getNumChildren() == 0;
			}
		});
		
		

		// And now remove them
		for(SceneQueryResult sqr : result) {
			sqr.node.remove();
		}

		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
}
 
